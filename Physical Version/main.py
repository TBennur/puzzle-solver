from recognition.imageControl import createPieces
from placement.puzzleControl import placePieces

puzzleNumber = 2
similarityForNormalization = 27
size = 250
minimumArea = 11000
margin = 5
usedForEdge = 3
filledForBox = 0.94
spaceForEdge = 27
maxForInner = 0.78
filledForInner = 0.60
scanningArea = 3
widthIncentiveForEdge = 0
heightIncentiveForEdge = 0
widthIncentive = 0.02
heightIncentive = 0.02
bottomPercentage = 0.96
offset = [0, -500]

def Main():
    pieces = createPieces(puzzleNumber, similarityForNormalization, size, minimumArea, margin, usedForEdge, filledForBox, spaceForEdge, filledForInner, maxForInner, scanningArea)
    print("Analysis Complete!")
    puzzle = placePieces(pieces, widthIncentiveForEdge, heightIncentiveForEdge, widthIncentive, heightIncentive, bottomPercentage, offset)
    print("Placement Complete!")
    puzzle.finalizePuzzle()
    print("Movement Plan Generated!")

Main()