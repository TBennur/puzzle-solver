from recognition.imageAlgorithms import makeArray, innerBox, sidesSensor, sideContours, countPieces
from general.classes import Piece


def createPieces(number, similarity, size, minimumArea, margin, usedForEdge, filledForBox, filledForEdge, filledForInner, maxForInner, scanningArea):
    counted = countPieces(number, similarity, size, minimumArea, margin)
    piecesList = []
    print("Piece Counting Complete!")
    print(str(counted[2]) + " Pieces Detected!")
    for i in range(counted[2]):
        pieceInformation = []
        stem = "puzzles/Puzzle" + str(number) + "/Piece" + str(i + 1)
        pieceInformation.append(stem)
        pieceInformation.append([size, size])
        rotated = makeArray(counted[0][i], counted[0][i], 0, similarity, scanningArea, counted[3], [[0, 0, 0]], [], [], size, size)
        img = rotated[0]
        pieceInformation.append(rotated[2])
        pieceInformation.append(innerBox(rotated[0], rotated[3], pieceInformation[1][0], pieceInformation[1][1], filledForBox))
        pieceInformation.append(sidesSensor(rotated[0], pieceInformation[3][0], pieceInformation[3][1], usedForEdge, pieceInformation[1][0], pieceInformation[1][1], filledForEdge, filledForInner, maxForInner))
        pieceInformation.append(sideContours(rotated[0], pieceInformation[3][0], pieceInformation[3][1], pieceInformation[4], pieceInformation[1][0], pieceInformation[1][1]))
        output = Piece(id = (i + 1), type = pieceInformation[4][0], image = img, size = pieceInformation[1], dimensions = [int(pieceInformation[3][2][0]), int(pieceInformation[3][2][1])], rotated = pieceInformation[2], outer = [int(pieceInformation[3][0][1]), int(pieceInformation[3][0][3]), int(pieceInformation[3][0][0]), int(pieceInformation[3][0][2])], inner = [int(pieceInformation[3][1][1]), int(pieceInformation[3][1][3]), int(pieceInformation[3][1][0]), int(pieceInformation[3][1][2])], sides = [pieceInformation[4][1], pieceInformation[4][2],pieceInformation[4][3], pieceInformation[4][4]], contour = pieceInformation[5], currentPosition = counted[1][i])
        pieceInformation.clear()
        piecesList.append(output)
        print("Finished Piece " + str(i + 1) + "!")
    return piecesList