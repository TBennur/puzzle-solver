from PIL import Image
import numpy as np
from general.utilities import quickArray, isColor, edgeCount, unpackArray, packArray, isUsed


def countPieces(number, similarity, size, minimum, margin):
    background = []
    stem = "puzzles/Puzzle" + str(number) + ".jpg"
    img = Image.open(stem)
    img.load()
    data = np.asarray(img, dtype= "float32")
    dimensions = [len(data), len(data[0])]
    values = unpackArray(data, dimensions[0], dimensions[1])
    background.append([values[0][0], values[0][1], values[0][2]])
    background.append([values[dimensions[0] * dimensions[1] - 1][0], values[dimensions[0] * dimensions[1] - 1][1], values[dimensions[0] * dimensions[1] - 1][2]]) 
    normalized = normalizeArray(values, dimensions[0], dimensions[1], similarity, background)
    values = normalized[0]
    usedRanges = []
    initialPositions = []
    images = []
    for i in range(dimensions[0]):
        for j in range(dimensions[1]):
            if([values[i * dimensions[0] + j][0], values[i * dimensions[0] + j][1], values[i * dimensions[0] + j][2]] == background[0]):
                continue
            elif isUsed(j, i, usedRanges):
                continue
            else:
                usedRanges.append(findArea(i, j, values, dimensions, background, margin))
                if((((usedRanges[len(usedRanges) - 1][1] - usedRanges[len(usedRanges) - 1][0]) * (usedRanges[len(usedRanges) - 1][3] - usedRanges[len(usedRanges) - 1][2])) > minimum) == False):
                    usedRanges.remove(usedRanges[len(usedRanges) - 1])
                else:
                    images.append(createImage(usedRanges[len(usedRanges) - 1], size, values, dimensions))
                    initialPositions.append([(usedRanges[len(usedRanges) - 1][0] + usedRanges[len(usedRanges) - 1][1])/2, (usedRanges[len(usedRanges) - 1][2] + usedRanges[len(usedRanges) - 1][3])/2])
    return([images, initialPositions, len(usedRanges), background])    

def findArea(i, j, values, dimensions, background, margin):
    initialI = i
    lastRow = [j]
    currentRow = []
    maxJ = j
    minJ = j
    while(True):
        if(len(lastRow) == 0):
            break
        i = i + 1
        j = min(lastRow)
        while(True):
            j = j - 1
            if([values[i * dimensions[0] + j][0], values[i * dimensions[0] + j][1], values[i * dimensions[0] + j][2]] != background[0]):
                if(j < minJ):
                    minJ = j
                currentRow.append(j)
            else:
                break
        j = min(lastRow)
        for x in range(max(lastRow) - min(lastRow) + 1):
            if([values[i * dimensions[0] + j + x][0], values[i * dimensions[0] + j + x][1], values[i * dimensions[0] + j + x][2]] != background[0]):
                currentRow.append(j + x)
            else:
                continue
        j = max(lastRow)
        while(True):
            j = j + 1
            if([values[i * dimensions[0] + j][0], values[i * dimensions[0] + j][1], values[i * dimensions[0] + j][2]] != background[0]):
                if(j > maxJ):
                    maxJ = j
                currentRow.append(j)    
            else:
                break
        lastRow = currentRow
        currentRow = []
    return([minJ - margin, maxJ + margin, initialI - margin, i + margin])
    
def createImage(border, size, values, totalDimensions):
    pieceImage = np.zeros(((size * size), 3))
    for i in range(size):
        for j in range(size):
            if(i < int((size - (border[3] - border[2]))/2)  or i > int((size + (border[3] - border[2]))/2) or j < int((size - (border[1] - border[0]))/2) or j > int((size + (border[1] - border[0]))/2)):
                pieceImage[i * size + j][0] = values[0][0]
                pieceImage[i * size + j][1] = values[0][1]
                pieceImage[i * size + j][2] = values[0][2]
            else:
                pieceImage[i * size + j][0] = values[(i + border[2] - int((size - (border[3] - border[2]))/2)) * totalDimensions[0] + j + border[0] - int((size - (border[1] - border[0]))/2)][0]
                pieceImage[i * size + j][1] = values[(i + border[2] - int((size - (border[3] - border[2]))/2)) * totalDimensions[0] + j + border[0] - int((size - (border[1] - border[0]))/2)][1]
                pieceImage[i * size + j][2] = values[(i + border[2] - int((size - (border[3] - border[2]))/2)) * totalDimensions[0] + j + border[0] - int((size - (border[1] - border[0]))/2)][2]
    section = packArray(pieceImage, size, size)
    return Image.fromarray(section, "RGB")

def normalizeArray(values, height, width, similarity, background):
    colors = 2
    newBackground = background
    for i in range(height):
        for j in range(width):
            isUsed = False
            value1 = values[((i * width) + j)][0]
            value2 = values[((i * width) + j)][1]
            value3 = values[((i * width) + j)][2]
            colors_used = 0
            for k in range(colors):
                if(((value1 - background[k][0] <= similarity) and (value1 - background[k][0] >= -similarity)) and ((value2 - background[k][1] <= similarity) and (value2 - background[k][1] >= -similarity)) and ((value3 - background[k][2] <= similarity) and (value3 - background[k][2] >= -similarity))):
                    values[((i * width) + j)][0] = background[k][0]
                    values[((i * width) + j)][1] = background[k][1]
                    values[((i * width) + j)][2] = background[k][2]
                    isUsed = True
                    break
                else:
                    colors_used += 1
            if(colors_used == colors and isUsed == False):
                newBackground.append([value1, value2, value3])
                colors += 1
    return([values, newBackground])

def renormalizeArray(values, height, width, shadesOfBlack, background):
    for i in range(height):
        for j in range(width):
            value1 = values[((i * width) + j)][0]
            value2 = values[((i * width) + j)][1]
            value3 = values[((i * width) + j)][2]
            if(isColor(value1, value2, value3, shadesOfBlack) == True):
                values[((i * width) + j)][0] = background[0][0]
                values[((i * width) + j)][1] = background[0][1]
                values[((i * width) + j)][2] = background[0][2]
    return values

def rotationInformation(values, repetitions, height, width, borderList, scanningArea, outerArea, background, shadesOfBlack):
    if(repetitions == 0):
        originalDimensions = [0, width, 0, height]
    else:
        originalDimensions = [int(borderList[(repetitions - 1)][2] - scanningArea), int(borderList[(repetitions - 1)][3] + scanningArea), int(borderList[(repetitions - 1)][1]- scanningArea), int(borderList[(repetitions - 1)][0] + scanningArea)]
        if(originalDimensions[0] < 0 or originalDimensions[1] > width or originalDimensions[2] < 0 or originalDimensions[3] > height):
            originalDimensions = [0, width, 0, height]
    left = width
    right = 0
    top = height
    bottom = 0
    for j in range(originalDimensions[2], originalDimensions[3]):
        for k in range(originalDimensions[0], originalDimensions[1]):
            i = j * width + k
            if(i >= height * width or i < -1):
                return([(height * width), [height, 0, 0, width]])
            elif([values[i][0], values[i][1], values[i][2]] == background[0] or isColor(values[i][0], values[i][1], values[i][2], shadesOfBlack) == True):
                continue
            else:
                if(j == height or j == 0 or k == width or k == 0):
                    return([(height * width), [height, 0, 0, width]])
                else:
                    if(k > right):
                        right = k
                    elif(k < left):
                        left = k
                    if(j > bottom):
                        bottom = j
                    elif(j < top):
                        top = j                    
                    else:
                        continue
    area = (bottom - top) * (right - left)
    dimensions = [bottom, top, left, right]
    return([area, dimensions])

def makeArray(image, img, repetitions, similarity, scanningArea, background, shadesOfBlack, outerArea, borderList, height, width):
    data = np.asarray(img, dtype= "float32")
    values = unpackArray(data, height, width)
    if(repetitions == 0):
        shadesOfBlack.append([values[0][0], values[0][1], values[0][2]]) 
    if(repetitions == 90):
        return values
    rotated = rotationInformation(values, repetitions, height, width, borderList, scanningArea, outerArea, background, shadesOfBlack)
    outerArea.append(rotated[0])
    borderList.append(rotated[1])
    return makeImage(image, values, repetitions, similarity, scanningArea, background, shadesOfBlack, outerArea, borderList, height, width)
                
def makeImage(image, values, repetitions, similarity, scanningArea, background, shadesOfBlack, outerArea, borderList, height, width):
    repetitions += 1
    if(repetitions < 90):
        img = image.rotate(repetitions)
        return makeArray(image, img, repetitions, similarity, scanningArea, background, shadesOfBlack, outerArea, borderList, height, width)
    else:
        img = image.rotate(outerArea.index(min(outerArea)))
        values = renormalizeArray(makeArray(image, img, repetitions, similarity, scanningArea, background, shadesOfBlack, outerArea, borderList, height, width), height, width, shadesOfBlack, background)
        section = packArray(values, height, width)
        img = Image.fromarray(section, "RGB")        
        return([img, min(outerArea), outerArea.index(min(outerArea)), borderList[outerArea.index(min(outerArea))]]) 

def innerBox(image, border, height, width, filledForBox):
    original = [border[0], border[1], border[2], border[3]]
    filled = [False, False, False, False, 0]
    while(filled[4] < 4):
        filled = edgeCount([edgeSensor(image, border[3], border[2], border[1], False, 0, height, width, filledForBox, 1.01), edgeSensor(image, border[0], border[1], border[3], True, 0, height, width, filledForBox, 1.01), edgeSensor(image, border[3], border[2], border[0], False, 0, height, width, filledForBox, 1.01), edgeSensor(image, border[0], border[1], border[2], True, 0, height, width, filledForBox, 1.01)])
        if(filled[0] == False):
            border[1] += 1
        if(filled[1] == False):
            border[3] -= 1
        if(filled[2] == False):
            border[0] -= 1
        if(filled[3] == False):
            border[2] += 1
    for i in range(4):
        filled = edgeCount([edgeSensor(image, border[3], border[2], border[1], False, 0, height, width, filledForBox, 1.01), edgeSensor(image, border[0], border[1], border[3], True, 0, height, width, filledForBox, 1.01), edgeSensor(image, border[3], border[2], border[0], False, 0, height, width, filledForBox, 1.01), edgeSensor(image, border[0], border[1], border[2], True, 0, height, width, filledForBox, 1.01)])
        if(i == 0 or i == 3):
            direction = 1
        else:
            direction = -1
        repetitions = -1
        while(filled[4] == 4):
            filled = edgeCount([edgeSensor(image, border[3], border[2], border[1], False, 0, height, width, filledForBox, 1.01), edgeSensor(image, border[0], border[1], border[3], True, 0, height, width, filledForBox, 1.01), edgeSensor(image, border[3], border[2], border[0], False, 0, height, width, filledForBox, 1.01), edgeSensor(image, border[0], border[1], border[2], True, 0, height, width, filledForBox, 1.01)])
            if(filled[0] == True and filled[1] == True and filled[2] == True and filled[3] == True):
                border[i] += direction
            repetitions += 1
        if(repetitions > 0):
            border[i] -= direction
    return([original, border, [original[0] - original[1], original[3] - original[2]]])

def sidesSensor(image, border, inner, shift, height, width, spaceForEdge, filledForInner, maxForInner):
    sides = []
    edges = edgeCount([abs(int(inner[1] - border[1])) < spaceForEdge, abs(int(inner[3] - border[3])) < spaceForEdge, abs(int(inner[0] - border[0])) < spaceForEdge, abs(int(inner[2] - border[2])) < spaceForEdge])
    filled = [edgeSensor(image, inner[3], inner[2], border[1], False, int(inner[1] - border[1]), height, width, filledForInner, maxForInner), edgeSensor(image, inner[0], inner[1], border[3], True, int(inner[3] - border[3]), height, width, filledForInner, maxForInner), edgeSensor(image, inner[3], inner[2], border[0], False, int(inner[0] - border[0]), height, width, filledForInner, maxForInner), edgeSensor(image, inner[0], inner[1], border[2], True, int(inner[2] - border[2]), height, width, filledForInner, maxForInner)]
    for i in range(4):
        if(edges[i] == True):
            sides.append("e")
        elif(filled[i] == True):
            sides.append("i")            
        else:
            sides.append("o")    
    if edges[4] == 2:
        return(["Corner", sides[0], sides[1], sides[2], sides[3]])        
    elif edges[4] == 1:
        return(["Edge", sides[0], sides[1], sides[2], sides[3]])    
    else:
        return(["Piece", sides[0], sides[1], sides[2], sides[3]])

def edgeSensor(image, border1, border2, border3, vertical, shift, height, width, filledForEdge, maxForInner):
    values = quickArray(image, height, width)
    if(shift != 0):
        direction = int(abs(shift)/shift)
    else:
        direction = 0
    filled = 0;
    for i in range(int(border2), int(border1 + 1)):
        for j in range((abs(shift) + 1)):
            if(vertical == True):
                if([values[(i * width) + direction * j + int(border3)][0], values[(i * width) + direction * j + int(border3)][1], values[(i * width) + direction * j + int(border3)][2]] != [values[0][0], values[0][1], values[0][2]]):
                    filled += 1
            else:
                if([values[((direction * j + int(border3)) * width) + i][0], values[((direction * j + int(border3)) * width) + i][1], values[((direction * j + int(border3)) * width) + i][2]] != [values[0][0], values[0][1], values[0][2]]):
                    filled += 1
    if(maxForInner >= (filled / ((border1 - border2 + 1) * (abs(shift) + 1))) >= filledForEdge):
        return True
    else:
        return False

def sideContour(image, border1, border2, border3, vertical, side, shift, height, width):
    if side == "e":
        return ""
    else:
        values = quickArray(image, height, width)
        contour = ""
        if(shift != 0):
            direction = int(abs(shift)/shift)
        else:
            direction = 0
        for i in range(int(border2), int(border1 + 1)):
            for j in range((abs(shift) + 1)):
                if(vertical == True):
                    if([values[(i * width) + direction * (abs(shift) - j) + int(border3)][0], values[(i * width) + direction * (abs(shift) - j) + int(border3)][1], values[(i * width) + direction * (abs(shift) - j) + int(border3)][2]] != [values[0][0], values[0][1], values[0][2]]):
                        if(j == abs(shift)):
                            contour += (str(j + 1) + "/")
                        continue
                    else:
                        contour += (str(j) + "/")
                        break
                else:
                    if([values[((direction * (abs(shift) - j) + int(border3)) * width) + i][0], values[((direction * (abs(shift) - j) + int(border3)) * width) + i][1], values[((direction * (abs(shift) - j) + int(border3)) * width) + i][2]] != [values[0][0], values[0][1], values[0][2]]):
                        if(j == abs(shift)):
                            contour += (str(j) + "/")
                        continue
                    else:
                        contour += (str(j) + "/")
                        break
        return contour

def sideContours(image, border, inner, sides, height, width):
    top = sideContour(image, inner[3], inner[2], border[1], False, sides[1], int(inner[1] - border[1]), height, width)
    right = sideContour(image, inner[0], inner[1], border[3], True, sides[2], int(inner[3] - border[3]), height, width)
    bottom = sideContour(image, inner[3], inner[2], border[0], False, sides[3], int(inner[0] - border[0]), height, width)
    left = sideContour(image, inner[0], inner[1], border[2], True, sides[4], int(inner[2] - border[2]), height, width)
    return([top, right, bottom, left])