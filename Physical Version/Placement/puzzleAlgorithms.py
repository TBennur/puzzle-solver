from general.utilities import sideMatch, matchAmount, reverseContour, packArray
from PIL import Image
import numpy as np

def determineDimensions(pieces):
    p = 0
    e = 0
    for x in range(len(pieces)):
        if(pieces[x].type == "Edge"):
            e += 1
        elif(pieces[x].type == "Piece"):
            p += 1
        else:
            continue
    return([int(2 + (e + np.sqrt(e * e - 16 * p)) / 4), int(2 + (e - np.sqrt(e * e - 16 * p)) / 4)])
    
def arrangePieces(pieces, puzzle, widthIncentiveForEdge, heightIncentiveForEdge, widthIncentive, heightIncentive):
    used = []
    rotation = []
    rotated = []
    for x in range(puzzle.height):
        if(x < puzzle.height):
            for y in range(puzzle.width):
                if(x == 0):
                    if(y == 0):
                        new_Piece = findPiece(0, pieces, "Corner", ["e", "e"], ["", ""], used, widthIncentiveForEdge, heightIncentiveForEdge)
                    elif(y == puzzle.width - 1):
                        new_Piece = findPiece(1, pieces, "Corner", ["e", pieces[used[puzzle.width * x + y - 1]].sides[1]], ["", pieces[used[puzzle.width * x + y - 1]].contour[1]], used, widthIncentiveForEdge, heightIncentiveForEdge)    
                    else:
                        new_Piece = findPiece(1, pieces, "Edge", ["e", pieces[used[puzzle.width * x + y - 1]].sides[1]], ["", pieces[used[puzzle.width * x + y - 1]].contour[1]], used, widthIncentiveForEdge, heightIncentiveForEdge)                        
                    used.append(new_Piece[0])
                    rotation.append(new_Piece[1])
                    rotated.append(new_Piece[2])
                elif(x < puzzle.height - 1):
                    if(y == 0):
                        new_Piece = findPiece(2, pieces, "Edge", [pieces[used[puzzle.width * (x - 1) + y]].sides[2], "e"], [pieces[used[puzzle.width * (x - 1) + y]].contour[2], ""], used, widthIncentiveForEdge, heightIncentiveForEdge)
                    elif(y == puzzle.width - 1):
                        new_Piece = findPiece(3, pieces, "Edge", [pieces[used[puzzle.width * (x - 1) + y]].sides[2], pieces[used[puzzle.width * x + y - 1]].sides[1]], [pieces[used[puzzle.width * (x - 1) + y]].contour[2], pieces[used[puzzle.width * x + y - 1]].contour[1]], used, widthIncentiveForEdge, heightIncentiveForEdge)
                    else:
                        new_Piece = findPiece(3, pieces, "Piece", [pieces[used[puzzle.width * (x - 1) + y]].sides[2], pieces[used[puzzle.width * x + y - 1]].sides[1]], [pieces[used[puzzle.width * (x - 1) + y]].contour[2], pieces[used[puzzle.width * x + y - 1]].contour[1]], used, widthIncentive, heightIncentive)
                    used.append(new_Piece[0])
                    rotation.append(new_Piece[1])
                    rotated.append(new_Piece[2])
                else:
                    if(y == 0):
                        new_Piece = findPiece(2, pieces, "Corner", [pieces[used[puzzle.width * (x - 1) + y]].sides[2], "e"], [pieces[used[puzzle.width * (x - 1) + y]].contour[2], ""], used, widthIncentiveForEdge, heightIncentiveForEdge)
                    elif(y == puzzle.width - 1):
                        new_Piece = findPiece(4, pieces, "Corner", [pieces[used[puzzle.width * (x - 1) + y]].sides[2], pieces[used[puzzle.width * x + y - 1]].sides[1]], [pieces[used[puzzle.width * (x - 1) + y]].contour[2], pieces[used[puzzle.width * x + y - 1]].contour[1]], used, widthIncentive, heightIncentive)
                    else:
                        new_Piece = findPiece(4, pieces, "Edge", [pieces[used[puzzle.width * (x - 1) + y]].sides[2], pieces[used[puzzle.width * x + y - 1]].sides[1]], [pieces[used[puzzle.width * (x - 1) + y]].contour[2], pieces[used[puzzle.width * x + y - 1]].contour[1]], used, widthIncentive, heightIncentive)
                    used.append(new_Piece[0])
                    rotation.append(new_Piece[1])
                    rotated.append(new_Piece[2])
            puzzle.addRow(used[puzzle.width * x: puzzle.width * (x + 1)], rotation[puzzle.width * x: puzzle.width * (x + 1)], rotated[puzzle.width * x: puzzle.width * (x + 1)])
    return puzzle

def findPiece(findType, pieces, pieceType, edges, contours, used, widthIncentive, heightIncentive):
    possibilities = []
    accuracy = []
    if(findType == 0):
        for x in range(len(pieces)):
            if(pieces[x].type == "Corner"):
                for a in range(4):
                    if(sideMatch(pieces[x].sides[0], edges[0], pieces[x].sides[3], edges[1])):
                        return([x, a, pieces[x].rotated, "Corner"])
                    else:
                        pieces[x].rotate()
    if(findType == 1):
        for x in range(len(pieces)):
            shouldBreak = False
            for y in range(len(used)):
                if(x == used[y]):
                    shouldBreak = True
            if(shouldBreak == True):
                continue
            if(pieces[x].type == pieceType):
                rotations = 0
                for a in range(4):
                    if(sideMatch(pieces[x].sides[0], edges[0], pieces[x].sides[3], edges[1])):
                        possibilities.append([x, a, pieces[x].rotated, pieces[x].type])
                        accuracy.append(min(contourMatch(pieces[x].contour[3], contours[1]), contourMatch(pieces[x].contour[3], reverseContour(contours[1]))) * (1 + abs(len(pieces[x].contour[3]) - len(contours[1])) * heightIncentive))
                        pieces[x].rotate()
                    else:
                        rotations += 1
                        pieces[x].rotate()
    if(findType == 2):
        for x in range(len(pieces)):
            shouldBreak = False
            for y in range(len(used)):
                if(x == used[y]):
                    shouldBreak = True
            if(shouldBreak == True):
                continue
            if(pieces[x].type == pieceType):
                rotations = 0
                for a in range(4):
                    if(sideMatch(pieces[x].sides[0], edges[0], pieces[x].sides[3], edges[1])):
                        possibilities.append([x, a, pieces[x].rotated, pieces[x].type])
                        accuracy.append(min(contourMatch(pieces[x].contour[0], contours[0]), contourMatch(pieces[x].contour[0], reverseContour(contours[0]))) * (1 + abs(len(pieces[x].contour[0]) - len(contours[0])) * widthIncentive))
                        pieces[x].rotate()
                    else:
                        rotations += 1
                        pieces[x].rotate()
    if(findType == 3):
        for x in range(len(pieces)):
            shouldBreak = False
            for y in range(len(used)):
                if(x == used[y]):
                    shouldBreak = True
            if(shouldBreak == True):
                continue
            if(pieces[x].type == pieceType):
                rotations = 0
                for a in range(4):
                    if(sideMatch(pieces[x].sides[0], edges[0], pieces[x].sides[3], edges[1])):
                        if(pieceType == "Edge" and pieces[x].sides[1] != "e"):
                            pieces[x].rotate()
                            continue
                        possibilities.append([x, a, pieces[x].rotated, pieces[x].type])
                        accuracy.append(min(contourMatch(pieces[x].contour[0], contours[0]), contourMatch(pieces[x].contour[0], reverseContour(contours[0]))) * (1 + abs(len(pieces[x].contour[0]) - len(contours[0])) * widthIncentive) + min(contourMatch(pieces[x].contour[3], contours[1]), contourMatch(pieces[x].contour[3], reverseContour(contours[1]))) * (1 + abs(len(pieces[x].contour[3]) - len(contours[1])) * heightIncentive))
                        pieces[x].rotate()
                    else:
                        rotations += 1
                        pieces[x].rotate()
    if(findType == 4):
        for x in range(len(pieces)):
            shouldBreak = False
            for y in range(len(used)):
                if(x == used[y]):
                    shouldBreak = True
            if(shouldBreak == True):
                continue
            if(pieces[x].type == pieceType):
                rotations = 0
                for a in range(4):
                    if(sideMatch(pieces[x].sides[0], edges[0], pieces[x].sides[3], edges[1])):
                        if(pieceType == "Edge" and pieces[x].sides[2] != "e"):
                            pieces[x].rotate()
                            continue
                        possibilities.append([x, a, pieces[x].rotated, pieces[x].type])
                        accuracy.append(min(contourMatch(pieces[x].contour[0], contours[0]), contourMatch(pieces[x].contour[0], reverseContour(contours[0]))) * (1 + abs(len(pieces[x].contour[0]) - len(contours[0])) * widthIncentive) + min(contourMatch(pieces[x].contour[3], contours[1]), contourMatch(pieces[x].contour[3], reverseContour(contours[1]))) * (1 + abs(len(pieces[x].contour[3]) - len(contours[1])) * heightIncentive))
                        pieces[x].rotate()
                    else:
                        rotations += 1
                        pieces[x].rotate()
    for x in range(possibilities[accuracy.index(min(accuracy))][1]):
        pieces[possibilities[accuracy.index(min(accuracy))][0]].rotate()
    return possibilities[accuracy.index(min(accuracy))]

def contourMatch(oldSide, newSide):
    accuracy = []
    oldSide = oldSide.split("/")
    newSide = newSide.split("/")
    for x in range(abs(len(oldSide) - len(newSide)) + 1):
        if(len(oldSide) > len(newSide)):
            accuracy.append(matchAmount(oldSide[x:(len(newSide) + x)], newSide))
        elif(len(oldSide) < len(newSide)):
            accuracy.append(matchAmount(oldSide, newSide[x:(len(oldSide) + x)]))
        else:
            accuracy.append(matchAmount(oldSide, newSide))
    return min(accuracy)

def finalPosition(puzzle, pieces):
    xList = []
    yList = []
    for x in range(puzzle.height):
        positions = []
        initial = []
        for y in range(puzzle.width):
            if(x == 0):
                finaly = pieces[puzzle.arrangement[x][y]].dimensions[0] / 2
            else:
                finaly = pieces[puzzle.arrangement[x - 1][y]].finalPosition[1] + pieces[puzzle.arrangement[x - 1][y]].dimensions[0] / 2 + pieces[puzzle.arrangement[x][y]].dimensions[0] / 2
            if(y == 0):
                finalx = pieces[puzzle.arrangement[x][y]].dimensions[1] / 2
            else:
                finalx = pieces[puzzle.arrangement[x][y - 1]].finalPosition[0] + pieces[puzzle.arrangement[x][y - 1]].dimensions[1] / 2 + pieces[puzzle.arrangement[x][y]].dimensions[1] / 2
            positions.append([finalx, finaly])
            initial.append(pieces[puzzle.arrangement[x][y]].currentPosition)
            xList.append(finalx + pieces[puzzle.arrangement[x][y]].dimensions[1] / 2)
            yList.append(finaly + pieces[puzzle.arrangement[x][y]].dimensions[0] / 2)
            pieces[puzzle.arrangement[x][y]].setPositions([finalx, finaly], [x, y])
        puzzle.addFinalInformation(initial, positions, [int(max(yList)), int(max(xList))])
    
def makeDisplay(puzzle, pieces, bottomPercentage):
    puzzleImage = np.zeros(((int(puzzle.dimensions[0] * bottomPercentage) *  puzzle.dimensions[1]), 5))
    for y in range(int(puzzle.dimensions[0] * bottomPercentage)):
        for x in range(puzzle.dimensions[1]):
            pixelInfo = determinePiece(puzzle, pieces, x, y)
            puzzleImage[y * puzzle.dimensions[1] + x][0] = pixelInfo[0]
            puzzleImage[y * puzzle.dimensions[1] + x][1] = pixelInfo[1]
            puzzleImage[y * puzzle.dimensions[1] + x][2] = pixelInfo[2]
    puzzle.addImage(Image.fromarray(packArray(puzzleImage, int(puzzle.dimensions[0] * bottomPercentage), puzzle.dimensions[1]), "RGB"))

def determinePiece(puzzle, pieces, xCoordinate, yCoordinate):
    for x in range(puzzle.height):
        for y in range(puzzle.width):
            if((xCoordinate >= (puzzle.finalPositions[x][y][0] - pieces[puzzle.arrangement[x][y]].dimensions[1] / 2)) and (xCoordinate <= (puzzle.finalPositions[x][y][0] + pieces[puzzle.arrangement[x][y]].dimensions[1] / 2)) and (yCoordinate >= (puzzle.finalPositions[x][y][1] - pieces[puzzle.arrangement[x][y]].dimensions[0] / 2)) and (yCoordinate <= (puzzle.finalPositions[x][y][1] + pieces[puzzle.arrangement[x][y]].dimensions[0] / 2))):
                return([pieces[puzzle.arrangement[x][y]].image[(pieces[puzzle.arrangement[x][y]].outer[0] + int(yCoordinate - (puzzle.finalPositions[x][y][1] - pieces[puzzle.arrangement[x][y]].dimensions[0] / 2))) * pieces[puzzle.arrangement[x][y]].size[0] + pieces[puzzle.arrangement[x][y]].outer[3] + int(xCoordinate - (puzzle.finalPositions[x][y][0] - pieces[puzzle.arrangement[x][y]].dimensions[1] / 2))][0], pieces[puzzle.arrangement[x][y]].image[(pieces[puzzle.arrangement[x][y]].outer[0] + int(yCoordinate - (puzzle.finalPositions[x][y][1] - pieces[puzzle.arrangement[x][y]].dimensions[0] / 2))) * pieces[puzzle.arrangement[x][y]].size[0] + pieces[puzzle.arrangement[x][y]].outer[3] + int(xCoordinate - (puzzle.finalPositions[x][y][0] - pieces[puzzle.arrangement[x][y]].dimensions[1] / 2))][1], pieces[puzzle.arrangement[x][y]].image[(pieces[puzzle.arrangement[x][y]].outer[0] + int(yCoordinate - (puzzle.finalPositions[x][y][1] - pieces[puzzle.arrangement[x][y]].dimensions[0] / 2))) * pieces[puzzle.arrangement[x][y]].size[0] + pieces[puzzle.arrangement[x][y]].outer[3] + int(xCoordinate - (puzzle.finalPositions[x][y][0] - pieces[puzzle.arrangement[x][y]].dimensions[1] / 2))][2]])
    return([pieces[puzzle.arrangement[0][0]].image[0][0], pieces[puzzle.arrangement[0][0]].image[0][1], pieces[puzzle.arrangement[0][0]].image[0][2]])