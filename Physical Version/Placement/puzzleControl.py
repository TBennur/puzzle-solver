from placement.puzzleAlgorithms import arrangePieces, finalPosition, makeDisplay, determineDimensions
from general.classes import Puzzle


def placePieces(pieces, widthIncentiveForEdge, heightIncentiveForEdge, widthIncentive, heightIncentive, bottomPercentage, offset):
    dimensions = determineDimensions(pieces)
    print("Dimensions Determined!", dimensions)
    puzzle = Puzzle(pieceCount = len(pieces), height = dimensions[0], width = dimensions[1], offset = offset)
    arrangePieces(pieces, puzzle, widthIncentiveForEdge, heightIncentiveForEdge, widthIncentive, heightIncentive)
    print("Puzzle Solved!")
    finalPosition(puzzle, pieces)
    print("Final Positions Determined!")
    makeDisplay(puzzle, pieces, bottomPercentage)
    print("Composite Image Created!")
    return puzzle