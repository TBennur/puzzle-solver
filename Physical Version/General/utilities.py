import numpy as np

def isOpposite(oldSide, newSide):
    if(oldSide == "i" and newSide == "o"):
        return True
    elif(oldSide == "o" and newSide == "i"):
        return True
    elif(oldSide == "e" and newSide == "e"):
        return True
    return False
    
def sideMatch(oldSideOne, newSideOne, oldSideTwo, newSideTwo):
    if(isOpposite(oldSideOne, newSideOne) and isOpposite(oldSideTwo, newSideTwo)):
        return True
    else:
        return False

def matchAmount(oldContour, newContour):
    combined = []
    for i in range(len(newContour) - 1):
        combined.append(int(newContour[i]) + int(oldContour[i]))
    return np.std(np.array(combined).astype(np.int), axis = 0)/np.mean(np.array(combined).astype(np.int))
    
def reverseContour(contour):
    contour = contour[::-1]
    contour = contour[1:len(contour)] + "/"
    return contour

def unpackArray(data, height, width):
    values = np.zeros(((height * width), 3))
    for i in range(height * width):
        values[i][0] = data[((int) (i / width))][(i % width)][0]
        values[i][1] = data[((int) (i / width))][(i % width)][1]
        values[i][2] = data[((int) (i / width))][(i % width)][2]
    return values

def packArray(values, height, width):
    section = []
    for i in range(height):
        row = []
        for j in range(width):
            color = [int(values[((i * width) + j)][0]), int(values[((i * width) + j)][1]), int(values[((i * width) + j)][2])]
            row.append(color)
        section.append(row)
    return np.asarray(section).astype(np.uint8)

def quickArray(image, height, width):
    data = np.asarray(image, dtype= "float32")
    return unpackArray(data, height, width)

def edgeCount(border):
    edges = 0
    for i in range(len(border)):
        if(border[i] == True):
            edges += 1
    border.append(edges)
    return border 
    
def isColor(value1, value2, value3, shadesOfBlack):
    color = [value1, value2, value3]
    for i in range(len(shadesOfBlack)):
        if(color == shadesOfBlack[i]):
            return True
    return False

def isUsed(x, y, usedRanges):
    for n in range(len(usedRanges)):
        if(x >= usedRanges[n][0] and x <= usedRanges[n][1] and y >= usedRanges[n][2] and y <= usedRanges[n][3]):
            return True
    return False