from PIL import Image
import matplotlib.pyplot as plt
import numpy as np
from general.utilities import quickArray


class Piece():  
    def __init__(self, id: int = 0, type: str = "", image: Image = [], size: int = [0, 0], dimensions: int = [0, 0], rotated: int = 0, rotations: int = 0, outer: int = [0, 0, 0, 0], inner: int = [0, 0, 0, 0], sides: str = ["", "", "", ""], contour: str = ["", "", "", ""], currentPosition: int = [0, 0], scaledFinalPosition: int = [0, 0], finalPosition: int = [0, 0]):
        self.id = id
        self.type = type
        self.image = image
        self.size = size
        self.dimensions = dimensions
        self.rotated = rotated
        self.rotations = rotations
        self.outer = outer
        self.inner = inner
        self.sides = sides
        self.contour = contour
        self.currentPosition = currentPosition
        self.scaledFinalPostion = scaledFinalPosition
        self.finalPosition = finalPosition
    
    def rotate(self):
        self.rotations += 1
        savedSides = self.sides[0]
        savedContour = self.contour[0]
        for i in range(3):
            self.sides[i] = self.sides[i + 1]
            self.contour[i] = self.contour[i + 1]
        self.sides[3] = savedSides
        self.contour[3] = savedContour
        self.dimensions = self.dimensions[::-1]
        
    def setPositions(self, finalPosition, scaledFinalPosition):
        self.finalPosition = finalPosition
        self.scaledFinalPostion = scaledFinalPosition
        self.__setImage()
        self.__setDimensions()
        
    def __setDimensions(self):
        for _ in range(self.rotations % 4):
            savedOuter = self.outer
            savedInner = self.inner
            self.outer = [self.size[0] - savedOuter[1], savedOuter[2], self.size[1] - savedOuter[3], savedOuter[0]]
            self.inner = [self.size[0] - savedInner[1], savedInner[2], self.size[1] - savedInner[3], savedInner[0]]
        
    def __setImage(self):
        for _ in range(self.rotations % 4):
            self.image = self.image.rotate(90)
        data = np.asarray(self.image, dtype= "float32")
        self.image = quickArray(data, self.size[0], self.size[1])

        
class Puzzle():    
    def __init__(self, pieceCount: int = 0, height: int = 0, width: int = 0, offset: list = [0, 0], arrangement: list = [], rotations: list = [], rotated: list = [], initialPositions: list = [], finalPositions: list = [], dimensions: list = [], image: Image = [], plan: str = ""):
        self.pieceCount = pieceCount
        self.height = height
        self.width = width
        self.offset = offset
        self.arrangement = arrangement
        self.rotations = rotations        
        self.rotated = rotated
        self.initialPositions = initialPositions
        self.finalPositions = finalPositions
        self.dimensions = dimensions
        self.image = image
        self.plan = plan
        
    def addRow(self, arrangementRow, rotationsRow, rotatedRow):
        self.arrangement.append(arrangementRow)
        self.rotations.append(rotationsRow)
        self.rotated.append(rotatedRow)
    
    def addFinalInformation(self, initialPositions, finalPositions, dimensions):
        self.initialPositions.append(initialPositions)
        self.finalPositions.append(finalPositions)
        self.dimensions = dimensions
        
    def addImage(self, image):
        self.image = image
    
    def finalizePuzzle(self):
        self.__generatePlan()
        plt.imshow(self.image)
        
    def __generatePlan(self):
        plan = ""
        currentX = 0
        currentY = 0
        for x in range(self.height):
            for y in range(self.width):
                plan = plan + "[" + str(self.initialPositions[x][y][0] - currentX) + ", " + str(self.initialPositions[x][y][1] - currentY) + "] - "
                currentX = self.initialPositions[x][y][0]
                currentY = self.initialPositions[x][y][1]
                plan = plan + "R" + str(self.rotated[x][y] + 90 * self.rotations[x][y]) + " - "
                plan = plan + "[" + str(self.finalPositions[x][y][0] + self.offset[0] - currentX) + ", " + str(self.finalPositions[x][y][1] + self.offset[1] - currentY) + "] | "
                currentX = self.finalPositions[x][y][0] + self.offset[0]
                currentY = self.finalPositions[x][y][1] + self.offset[1]
        self.plan = plan[:len(plan) - 3]