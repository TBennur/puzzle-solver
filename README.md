# Overview:
This project has two distinct versions. The first version was built as a proof of concept and is called "Online Version", and solves 
jigsaw puzzles on the website *thejigsawpuzzles.com*. The current version is called "Physical Version", and solves actual jigsaw puzzles. 
This version, which is both more useful and more advanced, is described in detail below.

# How to Run This Package:
1. Clone this repository.
2. Run *main.py*.
3. The program will output an image of the solved puzzle; as well as generating a plan to physically solve the puzzle:

# How to Add Image Sets:
It is possible to use this package to solve your own jigsaw puzzles by cloning the repository and 
inputing new images, but a couple of adjustments must be made and a few constraints apply.
### Adjustments
- A new picture labeled "Puzzle**X**" should be created under the *puzzles* module, and should contain all of the puzzle's pieces. 
- The *puzzleNumber* parameter in the file *main.py* should be changed to **X**.
- Other parameters in *main.py* may need adjustment, as they are usually set to the most recent puzzle.
### Constraints
- All pieces must have exactly 4 perpendicular sides; pentagonal, trapezoidal or triangular pieces will not be processed correctly.
- There must be an equal number of pieces in each row of the puzzle.
- The image must be taken parallel to the ground for optimal placement.
- The background of the image should be different from all colors within the image.
- All pieces should be seperated, but still contained within the image.
- Shadows should be minimized.

# Dependencies
- Selenium 3.141.59
- Numpy 1.16.5
- Pillow 6.2.0
- Matplotlib 3.1.1

# Results:
These are examples of the final images generated by this program.

Solution to Puzzle #1:

![Result 1](GitLab Pictures/FinalResultOne.png)

Solution to Puzzle #2:

![Result 2](GitLab Pictures/FinalResultTwo.png)

# Video Explanation
[![Click for full video!](https://i.ytimg.com/vi/l836etgVQBU/hqdefault.jpg)](https://www.youtube.com/watch?v=l836etgVQBU)

# Processes: 
![Flow Diagram](GitLab Pictures/FlowDiagram.png)
### The technical specification for this project can be found [here](https://docs.google.com/document/d/13bux6dxg3f19Ldb5o1dRUfJjR9TJDiXr/edit?usp=sharing&ouid=117250225767048663556&rtpof=true&sd=true)