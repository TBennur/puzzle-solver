from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
import time

driver = webdriver.Chrome()
listOfPieces = []
puzzleInformation = []
pieceLocations = [50, 50]
url = "https://www.thejigsawpuzzles.com/Puzzle-of-the-Day/Homemade-Pancakes-With-Berries-jigsaw-puzzle?cutout=50classic"

class puzzlePiece():
    def __init__(self, id: int = 0, type: int = 0, height: int = 0, width: int = 0, idealX: int = 0, idealY: int = 0, currentX: int = 0, currentY: int = 0, rotation: float= 0, image: str = ""):
        self.id = id
        self.type = type
        self.height = height
        self.width = width
        self.idealX = idealX
        self.idealY = idealY
        self.currentX = currentX
        self.currentY = currentY
        self.rotation = rotation
        self.image = image
   
def puzzlePieceNumber():
    pieceNumber = 0
    while(True):
        variableStem = "//canvas[@id = 'p_fragment" + str(pieceNumber) + "']"
        try:
            driver.find_element_by_xpath(variableStem)
        except NoSuchElementException:
            break
        pieceNumber += 1
    return(pieceNumber)

def puzzleWidth():
    pieceNumber = 0
    while 1 + 1 == 2:
        variableStem = "//canvas[@class = 'smallGroupClass fragmentsCanvasClass p_fragment" + str(pieceNumber) + " edgeFrag']"
        try:
            driver.find_element_by_xpath(variableStem)
        except NoSuchElementException:
            break
        pieceNumber += 1   
    return(pieceNumber - 1)
    
def isCorner(x, numberOfPieces, width):
    if x == (0) or x == (width - 1) or x == (numberOfPieces - width) or x == (numberOfPieces - 1):
        return True
    else:
        return False

def isHorizontalEdge(x, numberOfPieces, width):
    if (x >= (0) and x <= (width - 1)) or (x >= (numberOfPieces -  width) and x <= (numberOfPieces - 1)):
        return True
    else:
        return False

def isVerticalEdge(x, width):
    if ((x) % width == 0) or ((x + 1) % width == 0):
        return True
    else:
        return False

def puzzleDimensions():
    numberOfPieces = puzzlePieceNumber()
    width = puzzleWidth()
    puzzleInformation.append(numberOfPieces)
    puzzleInformation.append(width)
    addPieces(numberOfPieces, width)

def findIdealX(x, width):
    while 1 + 1 == 2:
        if x > width:
            x = x - width
        else:
            return(x)

def findIdealY(x, idealX, width):
    idealY = int((x + width - idealX)/(width))
    return(idealY)
   
def getCurrentPosition(information, n):
    informationList = information.split(";")
    currentOne = informationList[n]
    currentTwo = currentOne.split()
    currentThree = currentTwo[1]
    currentFour = currentThree.split("p")
    currentX = currentFour[0]
    return(currentX)

def addPieces(numberOfPieces, width):
    for x in range(numberOfPieces):    
        if isCorner(x, numberOfPieces, width) == True:
            pieceType = 4
        elif isHorizontalEdge(x, numberOfPieces, width) == True:
            pieceType = 3
        elif isVerticalEdge(x, width) == True:
            pieceType = 2
        else:
            pieceType = 1
        idealX = findIdealX((x + 1), width)
        idealY = findIdealY(x, idealX, width) + 1
        variableStem = "//canvas[@id = 'p_fragment" + str(x) + "']"
        element = driver.find_element_by_xpath(variableStem)
        pieceHeight = int(element.get_attribute("height"))
        pieceWidth = int(element.get_attribute("width"))
        information = element.get_attribute("style")
        currentX = int(float(getCurrentPosition(information, 2)))
        currentY = int(float(getCurrentPosition(information, 3)))
        listOfPieces.append(puzzlePiece(id = x, type = pieceType, height = pieceHeight, width = pieceWidth, idealX = idealX, idealY = idealY, currentX = currentX, currentY = currentY))

def placeFirstPiece():
    origin = driver.find_element_by_xpath("//canvas[@id = 'p_fragment0']")
    newAttributes = "arguments[0].setAttribute('style','opacity: 1; position: absolute; left: 50px; top: 50px; width:" + str(listOfPieces[0].width) + "px; height: " + str(listOfPieces[0].height) + "px; z-index: 10;')"
    driver.execute_script(newAttributes, origin)

def placeAdditionalPieces():
    for x in range(1, puzzleInformation[0]):
        variableStem = "//canvas[@id = 'p_fragment" + str(x) + "']"
        origin = driver.find_element_by_xpath(variableStem)
        if listOfPieces[x].id % puzzleInformation[1] == 0:
            finalX = 50
            finalY = int(pieceLocations[(2 * x - (2 * (puzzleInformation[1]) - 1))]) + int((listOfPieces[(x - (puzzleInformation[1]))].height + listOfPieces[(x)].height)/2)
            newAttributes = "arguments[0].setAttribute('style','opacity: 1; position: absolute; left: " + str(finalX) + "px; top: " + str(finalY) + "px; width:" + str(listOfPieces[x].width) + "px; height: " + str(listOfPieces[x].height) + "px; z-index: 10;')"
            driver.execute_script(newAttributes, origin)
        else:
            finalX = int(pieceLocations[(2 * x - 2)]) + int((listOfPieces[(x - 1)].width + listOfPieces[(x)].width)/2)
            finalY = int(pieceLocations[(2 * x - 1)])
            newAttributes = "arguments[0].setAttribute('style','opacity: 1; position: absolute; left: " + str(finalX) + "px; top: " + str(finalY) + "px; width:" + str(listOfPieces[x].width) + "px; height: " + str(listOfPieces[x].height) + "px; z-index: 10;')"
            driver.execute_script(newAttributes, origin)
        pieceLocations.append(finalX)
        pieceLocations.append(finalY)

def Main():
    start = time.time()
    driver.get(url)
    try:
        WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.XPATH, "//canvas[@id = 'p_fragment0']")))
    finally:
        puzzleDimensions()
        time.sleep(1)
        placeFirstPiece()
        placeAdditionalPieces()
        end = time.time()
        print("It took %d seconds to solve a %d piece puzzle." % (end - start, puzzleInformation[0]))
        
Main()