from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

print("Opening Temporary Window for Data Collection")

driver = webdriver.Chrome()
listOfPieces = []
marginPercent = .20
puzzleInformation = []
url = "https://thejigsawpuzzles.com/Castles/Cheverny-Castle-Park-France-jigsaw-puzzle?cutout=20classic"
driver.get(url)

class puzzlePiece():
    def __init__(self, id: int = 0, type: int = 0, height: int = 0, width: int = 0, currentX: int = 0, currentY: int = 0):
        self.id = id
        self.type = type
        self.height = height
        self.width = width
        self.currentX = currentX
        self.currentY = currentY

def puzzlePieceNumber():
    pieceNumber = 0
    while 1 + 1 == 2:
        variableStem = "//canvas[@id = 'p_fragment" + str(pieceNumber) + "']"
        try:
            driver.find_element_by_xpath(variableStem)
        except NoSuchElementException:
            break
        pieceNumber += 1
    return(pieceNumber)

def puzzleWidth():
    pieceNumber = 0
    while 1 + 1 == 2:
        variableStem = "//canvas[@class = 'smallGroupClass fragmentsCanvasClass p_fragment" + str(pieceNumber) + " edgeFrag']"
        try:
            driver.find_element_by_xpath(variableStem)
        except NoSuchElementException:
            break
        pieceNumber += 1
    return(pieceNumber - 1)

def isCorner(x, numberOfPieces, width):
    if x == (0) or x == (width - 1) or x == (numberOfPieces - width) or x == (numberOfPieces - 1):
        return True
    else:
        return False

def isTopEdge(x, numberOfPieces, width):
    if (x >= (0) and x <= (width - 1)):
        return True
    else:
        return False

def isBottomEdge(x, numberOfPieces, width):
    if (x >= (numberOfPieces -  width) and x <= (numberOfPieces - 1)):
        return True
    else:
        return False

def isLeftEdge(x, width):
    if ((x) % width == 0):
        return True
    else:
        return False

def isRightEdge(x, width):
    if ((x + 1) % width == 0):
        return True
    else:
        return False

def typeTest(x, numberOfPieces, width):
    if isCorner(x, numberOfPieces, width) == True:
        return(6)
    elif isTopEdge(x, numberOfPieces, width) == True:
        return(5)
    elif isBottomEdge(x, numberOfPieces, width) == True:
        return(4)
    elif isLeftEdge(x, width) == True:
        return(3)
    elif isRightEdge(x, width) == True:
        return(2)
    else:
        return(1)

def getCurrentPosition(information, n):
    informationList = information.split(";")
    currentOne = informationList[n]
    currentTwo = currentOne.split()
    currentThree = currentTwo[1]
    currentFour = currentThree.split("p")
    currentX = currentFour[0]
    return(int(round(float(currentX))))

def addPieces(numberOfPieces, width):
    for x in range(numberOfPieces):    
        variableStem = "//canvas[@id = 'p_fragment" + str(x) + "']"
        element = driver.find_element_by_xpath(variableStem)
        pieceType = typeTest(x, numberOfPieces, width)
        pieceHeight = int(element.get_attribute("height"))
        pieceWidth = int(element.get_attribute("width"))
        information = element.get_attribute("style")
        currentX = int(getCurrentPosition(information, 2))
        currentY = int(getCurrentPosition(information, 3))
        listOfPieces.append(puzzlePiece(id = x, type = pieceType, height = pieceHeight, width = pieceWidth, currentX = currentX, currentY = currentY))
    averagePieceSize()
        
def averagePieceSize():
    totalHeight = 0
    totalWidth = 0
    for x in range(puzzleInformation[0]):
        totalHeight = totalHeight + listOfPieces[x].height
        totalWidth = totalWidth + listOfPieces[x].width
    xMargin = int((totalWidth/puzzleInformation[0]) * marginPercent)
    yMargin = int((totalHeight/puzzleInformation[0]) * marginPercent)
    puzzleInformation.append(xMargin)
    puzzleInformation.append(yMargin)
    
def puzzleDimensions():
    numberOfPieces = puzzlePieceNumber()
    width = puzzleWidth()
    puzzleInformation.append(numberOfPieces)
    puzzleInformation.append(width)
    addPieces(numberOfPieces, width)
    for x in range(0, len(listOfPieces)):
        puzzleInformation.append(listOfPieces[x])
    driver.close()
    return(puzzleInformation)