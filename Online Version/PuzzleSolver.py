from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import puzzlePieceAdder
import time

driver = webdriver.Chrome()
actions = ActionChains(driver)

listOfPieces = []
puzzleInformation = []
pieceLocations = [25, 25]
marginPercent = .20

url = "https://www.thejigsawpuzzles.com/Puzzle-of-the-Day/The-Churchill-Arms-London-jigsaw-puzzle?cutout=20classic"

class puzzlePiece():
    def __init__(self, id: int = 0, type: int = 0, height: int = 0, width: int = 0, currentX: int = 0, currentY: int = 0):
        self.id = id
        self.type = type
        self.height = height
        self.width = width
        self.currentX = currentX
        self.currentY = currentY

def pageSetup():
    driver.maximize_window()
    try:
        WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.XPATH, "//canvas[@id = 'p_fragment0']")))
    finally:
        deleteElements()
        enlarge = driver.find_element_by_id("p_fullScreen")
        actions.click(enlarge).perform()

def deleteElements():
    element = driver.find_element_by_id("p_timer")
    driver.execute_script("""var element = arguments[0];element.parentNode.removeChild(element);""", element)
    element = driver.find_element_by_id("p_showPreview")
    driver.execute_script("""var element = arguments[0];element.parentNode.removeChild(element);""", element)
    element = driver.find_element_by_id("gsHeader")
    driver.execute_script("""var element = arguments[0];element.parentNode.removeChild(element);""", element)
    element = driver.find_element_by_id("gsLeftLowerBlock")
    driver.execute_script("""var element = arguments[0];element.parentNode.removeChild(element);""", element)
    element = driver.find_element_by_id("gbSaveLoadBlock")
    driver.execute_script("""var element = arguments[0];element.parentNode.removeChild(element);""", element)
    element = driver.find_element_by_id("gsNavBar")
    driver.execute_script("""var element = arguments[0];element.parentNode.removeChild(element);""", element)
    element = driver.find_element_by_class_name("gbNavigatorMicroThums")
    driver.execute_script("""var element = arguments[0];element.parentNode.removeChild(element);""", element)    
    element = driver.find_element_by_class_name("gbPuzzleNameLabel")
    driver.execute_script("""var element = arguments[0];element.parentNode.removeChild(element);""", element)
    element = driver.find_element_by_class_name("gsContentDetail")
    driver.execute_script("""var element = arguments[0];element.parentNode.removeChild(element);""", element)

def reformat(unformattedData):
    for x in range(0, 4):
        puzzleInformation.append(unformattedData[x])
    for x in range(4, len(unformattedData)):
        listOfPieces.append(unformattedData[x])

def placePieces():
    origin = driver.find_element_by_xpath("//canvas[@id = 'p_fragment0']")
    endAttributes = "arguments[0].setAttribute('class','fragmentsCanvasClass p_fragment1 edgeFrag')"
    actions.click_and_hold(origin).move_by_offset((25 - listOfPieces[0].currentX), (25 - listOfPieces[0].currentY)).release(origin).perform()  
    driver.execute_script(endAttributes, origin)
    for x in range(1, puzzleInformation[0]):
        variableStem = "//canvas[@id = 'p_fragment" + str(x) + "']"
        if listOfPieces[x].type == 3:
            finalX = 25
            givenY = int(pieceLocations[(2 * x - (2 * (puzzleInformation[1]) - 1))]) + int((listOfPieces[(x - (puzzleInformation[1]))].height + listOfPieces[(x)].height)/2) - puzzleInformation[3]
            placeColumnPiece(finalX, givenY, variableStem, x)
        else:
            givenX = int(pieceLocations[(2 * x - 2)]) + int((listOfPieces[(x - 1)].width + listOfPieces[(x)].width)/2) - puzzleInformation[2]
            finalY = int(pieceLocations[(2 * x - 1)])
            placeRowPiece(givenX, finalY, variableStem, x)
    enlarge = driver.find_element_by_id("p_fullScreen")
    actions.click(enlarge).perform()
    actions.click(enlarge).perform()

def placeRowPiece(givenX, finalY, variableStem, x):
    origin = driver.find_element_by_xpath(variableStem)
    finalX = givenX
    endAttributes = "arguments[0].setAttribute('class','fragmentsCanvasClass p_fragment1 edgeFrag')"
    driver.execute_script(endAttributes, origin)
    pieceLocations.append(finalX)
    pieceLocations.append(finalY)
    
def placeColumnPiece(finalX, givenY, variableStem, x):
    origin = driver.find_element_by_xpath(variableStem)
    finalY = givenY
    endAttributes = "arguments[0].setAttribute('class','fragmentsCanvasClass p_fragment1 edgeFrag')"
    actions.click_and_hold(origin).move_by_offset((finalX - listOfPieces[0].currentX), (finalY - listOfPieces[0].currentY)).release(origin).perform()
    driver.execute_script(endAttributes, origin)
    pieceLocations.append(finalX)
    pieceLocations.append(finalY)

def Main():
    start = time.time()
    driver.get(url)
    pageSetup()
    unformattedData = puzzlePieceAdder.puzzleDimensions()
    reformat(unformattedData)
    placePieces()
    end = time.time()
    elapsedTime = end - start
    print("It took %d seconds to solve a %d piece puzzle" % (elapsedTime, puzzleInformation[0]))

Main()